<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'annie' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         ':VWaDo|VA6k2YL.G?S9PcTy3Wu:ww#5a4:>Q*Ltc><N$m_LO4gV}allOb|LXr8Hm' );
define( 'SECURE_AUTH_KEY',  'E}J3lGj^#ABOj1Zx8C:8[}ghF+rqE}rWxlBtER!nhWu0 ?KZytnUg5d,+*kT7}KG' );
define( 'LOGGED_IN_KEY',    'Z@ {cr{|*%SHF *6U`>f0WZ}pXrb^9%KT}13%AP xPi+P4BV9Ent/LK#hAviGqH3' );
define( 'NONCE_KEY',        'eh:w3-RI.9Bc`ho2Pr?.$-gwB;O//`Np|KB2meYeMY;4`Q5;27)zzaT~5:{TA^>e' );
define( 'AUTH_SALT',        'Qk)0fa/>?jE!6J4U#WPgG:v7:N`O{aw=C4ZrgpYk4{|WN<wurEKUa=y.8w&o1M*z' );
define( 'SECURE_AUTH_SALT', ');n8)jcZp+1UN~hiItv|ltBVw.F&GFVRY3[o^.;ak4j]_@+Q}ltR6H:Dj0ufLs/o' );
define( 'LOGGED_IN_SALT',   'jFaA}Zb1f+ps:uRb@w!FBX5IslvAXDyLW5ET^*mfO|A_|6g$ExzbV S7m0;&y5@~' );
define( 'NONCE_SALT',       '-hny]0-m,76<8O>*c^KeQmy|L[;WnX}EweCTJRr>sXXpu.$4%zjTq!I>T~KEk2Iz' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
